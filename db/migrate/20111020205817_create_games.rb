class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :playerA_id
      t.integer :playerB_id
      t.integer :winner_id
      t.integer :map_id
      t.references :match

      t.timestamps
    end
    add_index :games, :match_id
  end
end
