class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :teamA_id
      t.integer :teamB_id

      t.timestamps
    end
  end
end
