# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)

Admin.create({:email => 'starleague@mit.edu', :password => 'iamaleaf'})
Team.create({:name=>'mit', :email => 'user1@mit.edu', :password => 'password'})
Team.create({:name=>'harvard', :email => 'user2@mit.edu', :password => 'password'})
