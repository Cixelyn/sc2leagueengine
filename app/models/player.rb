class Player < ActiveRecord::Base
  belongs_to :team
  validates_presence_of :name, :code, :alias, :team

  def to_s
    "#{self.alias}.#{self.code} (#{self.name})"
  end

end
